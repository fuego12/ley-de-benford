import matplotlib.pyplot as plt
import numpy as np
import requests
import pandas as pd
import math


provincias = ["jujuy", "salta", "formosa", "misiones", "chaco", "corrientes", "entrerios", "catamarca", "tucuman",
              "cordoba", "santafe", "larioja", "sanluis", "sanjuan", "mendoza", "lapampa", "buenosaires", "neuquen",
              "rionegro", "chubut", "santacruz", "tierradelfuego", "santiagodelestero"]

def f(url):
  headers = {"User-agent": 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36'}
  respuesta = requests.get(url, headers=headers)
  all_tables = pd.read_html(respuesta.content, encoding = 'utf8')
  datos = pd.concat([all_tables[0], all_tables[1]])
  return datos

datos = []
ruta = "https://www.citypopulation.de/es/argentina/"



for provincia in provincias:
  datos.append(f(ruta + provincia + "/"))

df = pd.concat(datos)
df.reset_index(inplace = True)
df = df.drop(["index", "Nombre", "Estado", "Departamento", "Unnamed: 5", "Unnamed: 6"], axis = 1)



numeros = df.to_numpy().flatten(order='A')
inicios = []
for n in numeros:
  try:
    n = float(n)
    inicios.append(n // (10 ** int(math.log10(n))))
  except:
    pass



final = []
for i in range(1,10):
  final.append(inicios.count(i)/len(inicios))
final = np.array(final) * 100



plt.figure(figsize=(13,8))
plt.xlabel("Numeros")
plt.ylabel("Porcentaje")
plt.xticks(range(1, 10))

bar_width = 0.45

bar1 = plt.bar(np.arange(1,10)+bar_width/2, final, width = bar_width, label = 'Ciudades Argentina')
bar2 = plt.bar(np.arange(1,10)-bar_width/2, np.log10(1+(1/np.arange(1,10)))*100, width = bar_width, label = 'Ley de Benford')

for value in bar1:
    height = value.get_height()
    plt.text(value.get_x() + value.get_width()/2.,
             0.6*height, str(round(float(height),2)) + '%', ha='center', va='bottom')
for value in bar2:
    height = value.get_height()
    plt.text(value.get_x() + value.get_width()/2.,
             0.4*height, str(round(float(height),2)) + '%', ha='center', va='bottom')


plt.legend()
plt.show()
